# Installer linux (Ubuntu) sur chromebook

Dans les ateliers Kodo, on a souvent besoin d'un peu plus qu'un navigateur.
Or, sur les chromebooks, il est possible d'installer linux (Ubuntu) dans un environment [chroot](https://fr.wikipedia.org/wiki/Chroot).

Ce guide explique comment mettre à jour Chrome OS,
activer le mode développeur et ensuite installer [Ubuntu](https://fr.wikipedia.org/wiki/Ubuntu) pour tous les utilisateurs grâce à [Crouton](https://en.wikipedia.org/wiki/Crouton_(computing)).

## Mettre à jour Chrome OS (facultatif)

Pour mettre à jour Chrome OS, allez dans les paramètres via le menu en bas à droite.

Dans les paramètres, ouvrez le menu à gauche et cliquez sur *À propos de Chrome OS*.

Vérifiez que le système est à jour. Si ce n'est pas le cas, installez les mises à jour.

## Activer le mode développeur

**L'activation du mode développeur entraine la suppression des données présentes sur l'ordinateur.**

Redémarrez le chromebook en appuyant sur `échap`+`rafraichir`+`power`.
Ces touches sont les 1e, 4e et dernière de la première ligne de touches sur les Acer Chromebooks 14.

Si nécessaire, naviguez avec la flèches gauche ou droite pour passer le texte en français.

Appuyez sur `CTRL`+`D` pour afficher l'écran qui permet de désactiver la vérification du système d'exploitation.

Confirmez ensuite la désactivation en appuyant sur `Enter`.
Le mode développeur est activé.
L'ordinateur va redémarrer et effacer les données locales (ça peut prendre plusieurs minutes).

À chaque démarrage, un message indique que *la vérification du système est désactivée*.
Ce message est aussi accompagné de deux bips.
On ne peut pas passer ce message, il faut attendre qu'il disparaisse.

## Installer Ubuntu
Pour installer Ubuntu, téléchargez crouton avec ce lien [https://goo.gl/fd3zc] depuis le chromebook.

Ouvrez un terminal en appuyant sur les touches `CTRL`+`ALT`+`T`.

Dans le terminal, tapez `shell` et appuyez sur `enter`. Cette commande donne accès au [shell](https://fr.wikipedia.org/wiki/Interface_syst%C3%A8me) linux (interface en ligne de commande).

Crouton permet d'installer Ubuntu avec plusieurs types d'interfaces graphiques.
Afin d'utiliser une interface légère, on va installer Ubuntu avec [Xfce](https://fr.wikipedia.org/wiki/Xfce).

Pour cela, il faut utiliser la commande suivante (tapez `enter` à la fin) :

```
sudo sh ~/Downloads/crouton -t xfce
```

De nombreux fichiers vont être téléchargés et installés.
À la fin de l'installation, un nom d'utilisateur et un mot de passe seront demandés.

### Quelques informations sur la commande d'installation

- `sudo` active les droits administrateurs.
- `sh` indique que le shell va être utilisé pour exécuter le fichier qui suit.
- `~/Download/crouton` est le chemin vers le fichier qui a été téléchargé. `~` est un alias pour le répertoire de l'utilisateur.
- `-t xfce` indique qu'on installe la version Xfce d'Ubuntu. Pour avoir la liste des possibilités, il faut remplacer `xfce` par `list`.

## Lancer Ubuntu Xfce

Pour lancer Ubuntu Xfce depuis n'importe quel compte sur l'ordinateur, ouvrez un terminal dans Chrome OS en appuyant sur `CTRL`+`ALT`+`T`.

Ensuite, lancez la commande suivante (tapez `enter` à la fin) :

```
sudo startxfce4
```

Pour passer de linux à Chrome OS (ou inversément), utilisez les touches `CTRL`+`ALT`+`Précédent` (à côte de `échap`) et `CTRL`+`ALT`+`Suivant`.